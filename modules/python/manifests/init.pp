class python($packages = undef) {
    package {"build-essential":
        ensure => installed,
    }
    package {"python":
        ensure => installed,
    }

    package {"python-dev":
        ensure => installed,
        require => Package['build-essential'],
    }

    package {"python-pip":
        ensure => installed,
        require => Package['python'],
    }

}
