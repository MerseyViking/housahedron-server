define python::package() {
    package {$title:
        ensure => installed,
        require => Package['python-pip'],
        provider => pip,
    }
}