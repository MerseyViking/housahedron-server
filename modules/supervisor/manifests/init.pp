class supervisor($programs) {
    package {'supervisor':
        ensure => present,
    }

    file {'supervisord.conf':
        path => '/etc/supervisor/supervisord.conf',
        ensure => present,
        require => Package['supervisor'],
        mode => 0644,
        content => template('supervisor/supervisord.conf.erb'),
    }

    service {'supervisor':
        ensure => running,
        enable => true,
        hasrestart => true,
        restart => 'supervisorctl reread',
        subscribe => File['supervisord.conf'],
    }
}