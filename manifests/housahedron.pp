include base

package {'libevent-dev':
    ensure => present,
    before => Package['gevent']
}

class {'python': }

python::package {'gunicorn':
    before => Class['supervisor']
}

python::package {'gevent':}
python::package {'pymongo':}
python::package {'flask':}

class {'supervisor':
    programs => [
        {
            'name' => 'housahedron_server',
            'command' => '/usr/local/bin/gunicorn housahedron:app --bind unix:/tmp/housahedron.sock -w 4',
            'directory' => '/vagrant/',
        },
        {
            'name' => 'plumber',
            'command' => '/usr/bin/python plumber.py',
            'directory' => '/vagrant/',
        }
    ]
}

class {'apt':
    always_apt_update => false,
    before => Class['mongodb'],
}

class {'mongodb':
    enable_10gen => true,
    before => Class['supervisor'],
}

class {'nginx':
    require => Class['supervisor'],
}

nginx::resource::upstream { 'housahedron_server':
    ensure  => present,
    members => [
        'unix:/tmp/housahedron.sock fail_timeout=0',
    ],
}

nginx::resource::vhost {'housahedron':
    ensure => present,
    proxy => 'http://housahedron_server',
}

package {'python-pyassimp':
  ensure => present,
  before => Class['nginx']
}