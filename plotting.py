import sqlite3
import matplotlib.pyplot as pyplot
from dateutil import parser

levels = ('Floor', 'Level 1', 'Level 2', 'Ceiling')

locations = {u'3e072719-463a-4bdb-8d82-96efef815a2a': 'Hall',
             u'8e4f0ed4-7547-48ee-878d-2a6a5c55f7d4': 'Fireplace',
             u'a75d60d3-bc42-496c-8c8f-dec7bd40f727': 'TV',
             u'7dd319a7-b1bf-4315-880a-d4627c944a14': 'Back door',
             u'021556ef-342a-4726-8bf4-4a1706ab38f6': 'Pantry 1'}


def plot_by_level():
    for s in xrange(4):
        foo = pyplot.subplot(2, 2, s + 1)
        foo.set_title(levels[s])
        devices = cur.execute("SELECT uuid, metering_point_id FROM devices WHERE uuid LIKE ?", ('%0{0}'.format(s), )).fetchall()

        for device in devices:
            values = cur.execute("SELECT timestamp, value FROM measurements WHERE device_id = ? ORDER BY timestamp", (device[0],)).fetchall()
            x, y = zip(*values)

            pyplot.plot([parser.parse(xx) for xx in x], y, label=locations[device[1]])

        temperatures = cur.execute("SELECT observation_time, temperature FROM observations").fetchall()
        x, y = zip(*temperatures)
        pyplot.plot([parser.parse(xx) for xx in x], y, label='Ambient temperature')


def plot_by_location():
    s = 0

    for k in locations:
        foo = pyplot.subplot(3, 2, s + 1)
        foo.set_title(locations[k])
        devices = cur.execute("SELECT uuid, metering_point_id FROM devices WHERE metering_point_id = ? ORDER BY uuid", (k, )).fetchall()
        l = 0

        for device in devices:
            values = cur.execute("SELECT timestamp, value FROM measurements WHERE device_id = ? ORDER BY timestamp", (device[0],)).fetchall()
            x, y = zip(*values)

            pyplot.plot([parser.parse(xx) for xx in x], y, label=levels[l])
            l += 1

        temperatures = cur.execute("SELECT observation_time, temperature FROM observations").fetchall()
        x, y = zip(*temperatures)
        pyplot.plot([parser.parse(xx) for xx in x], y, label='Ambient temperature')
        s += 1


if __name__ == '__main__':
    conn = sqlite3.connect('temp/housahedron.db')
    cur = conn.cursor()

    plot_by_level()
    plot_by_location()

    conn.close()
    pyplot.legend()
    pyplot.gcf().autofmt_xdate()
    pyplot.show()
