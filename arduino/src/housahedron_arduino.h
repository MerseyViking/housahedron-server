/*
# housahedron_arduino.h
# Copyright (C) 2013 Housahedron
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HOUSAHEDRON_ARDUINO_H__
#define __HOUSAHEDRON_ARDUINO_H__

#define DEBUG 0
#define DEMO 0

#define REREGISTER_TIMEOUT 12
// The maximum for loseSomeTime() is 65535
// On the JeeNodes that have been tested, loseSomeTime() seems to add an extra second per minute, so this compensates
// for that which gives us a slightly more accurate 5 minute delay.
#define DEMO_SLEEP_TIME 1300
// For the first couple of minutes, take readings every 5 seconds or so so we
// can easily identify the Sadie box.
#define INITIAL_SAMPLES 24
// Times are multiplied by 5.
#define INITIAL_SLEEP_TIME 1000
#define SLEEP_TIME 59000
#define BATTERY_LOW_SLEEP_TIME 2000

#define HOUSAHEDRON_MAGIC 0x4A44
#define HOUSAHEDRON_VERSION_HIGH 0x00
#define HOUSAHEDRON_VERSION_LOW 0x03

#define RF_CHANNEL 111

#define JEENODE_PORT_1 4
#define JEENODE_PORT_2 5
#define JEENODE_PORT_3 6
#define JEENODE_PORT_4 7

#define MAX_SENSORS 8

#define EEPROM_MAGIC_OFFSET 0x40
#define EEPROM_VERSION_OFFSET 0x42
#define EEPROM_UUID_OFFSET 0x44

// When we're sending data from the base JeeNode to the base Arduino, we need to know whether it's log text or JSON, and when it has all been sent.
static const unsigned char SERIAL_START_LOG = 1;
static const unsigned char SERIAL_START_DATA = 2;
static const unsigned char SERIAL_STOP = 0x0A;

#define serial_log_P(format, ...)  fwrite(&SERIAL_START_LOG, 1, 1, &uartout);\
fprintf_P(&uartout, format, ##__VA_ARGS__);\
fwrite(&SERIAL_STOP, 1, 1, &uartout);

#define serial_data_P(format, ...)  fwrite(&SERIAL_START_DATA, 1, 1, &uartout);\
fprintf_P(&uartout, format, ##__VA_ARGS__);\
fwrite(&SERIAL_STOP, 1, 1, &uartout);


// NOTE: Enums are treated as int16s, so ALWAYS cast them to a byte before use.
enum sensor_type {
    TYPE_UNKNOWN,
    TYPE_TEMPERATURE,
    TYPE_HUMIDITY
};

enum commands {
    CMD_UNKNOWN,
    CMD_REGISTER,
    CMD_SEND_READING
};

#pragma pack(1)
struct sensor_payload {
  unsigned char sensor_id;
  unsigned char sensor_type;
  float sensor_value;
};

struct payload_packet {
    unsigned char command;
    byte uuid[16];
    char data[0];
};

struct node_metadata {
    byte num_sensors;
    byte sensor_types[MAX_SENSORS];
    byte battery_status;
};

void init_uart();
void init_node();
void generate_UUID(unsigned char* uuid);
void print_UUID(unsigned char* uuid, char* uuid_string);
bool is_housahedron();
int get_version();
void get_UUID(unsigned char* uuid);
void dump_EEPROM();
void fmtDouble(double val, byte precision, char *buf, unsigned bufLen = 0xffff);
unsigned fmtUnsigned(unsigned long val, char *buf, unsigned bufLen = 0xffff, byte width = 0);
void start_timer(unsigned long& timer);
bool check_timer(unsigned long& timer, unsigned long interval);

// See RFC 4648, section 5 on base64url encoding.
void base64url_encode(char* output, const uint8_t* input, const uint8_t input_length);
uint8_t base64url_encode_length(const uint8_t input_length);

// Enable the use of fprintf to print to the serial port.
extern FILE uartout;

#endif
