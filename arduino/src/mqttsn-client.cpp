/*
mqttsn-client.cpp
Copyright (C) 2013 Housahedron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>

#include "housahedron_arduino.h"
#include "mqttsn.h"
#include "mqttsn-client.h"
#include "mqttsn-messages.h"

#include <EEPROM.h>
#include <JeeLib.h>
#include <Ports.h>
#include <OneWire.h>

#define STATUS_LED_PIN 5
#define RESPONSE_TIMEOUT 5000

byte initial_samples = INITIAL_SAMPLES;
uint16_t sleep_time = INITIAL_SLEEP_TIME;
char bat_status = 0;
char client_id[24] = {0};
char battery_low_topic[60] = {0};
char temperature_topic[60] = {0};

void SadieMQTTSN::regack_handler(const msg_regack* msg) {
    MQTTSN::regack_handler(msg);

    uint8_t index;
    uint16_t topic_id = find_topic_id(battery_low_topic, index);

    if (topic_id != 0 && topic_id != 0xffff) {
        state = SadieMQTTSN::PUBLISHING;
    }
}

//void SadieMQTTSN::pingresp_handler() {
//    MQTTSN::pingresp_handler();
//
//    state = SadieMQTTSN::PUBLISHING;
//}

void SadieMQTTSN::timeout() {
#if DEBUG
    Serial.print("Timeout waiting for ");
    Serial.println(response_we_are_waiting_for());
    delay(100);
#endif
    state = SadieMQTTSN::REGISTERING;
    MQTTSN::timeout();
}

SadieMQTTSN client = SadieMQTTSN();

// For the DS18B20, the time between requesting a temperature and getting it depends on the precision of the ADC.
static const int ADC_delay[] = {94, 188, 375, 750};

static const byte sensor_control_pins[] = {A0, A1, A2, A3};

void enable_sensors() {
    digitalWrite(client.sensor_control_pin, HIGH);
    // Give the sensors time to get out of command mode.
    delay(10);
}

void disable_sensors() {
    digitalWrite(client.sensor_control_pin, LOW);
}

void low_power(uint16_t delay, uint8_t times) {
    rf12_sleep(RF12_SLEEP);
    disable_sensors();

    for (uint8_t i = 0; i < times; ++i) {
        Sleepy::loseSomeTime(delay);
    }
}

void take_readings() {
    enable_sensors();

    client.sensor_bus->reset();
    client.sensor_bus->write(0xCC);
    client.sensor_bus->write(0x44);

//    Sleepy::loseSomeTime(ADC_delay[client.precision]);
    // Because loseSomeTime() isn't very accurate, add a 10% margin.
    // It seems to return 1360(85.0 deg C) if it doesn't work.
//    delay(80);

    delay(ADC_delay[client.precision]);

    for (byte sensor_index = 0; sensor_index < client.num_sensors; ++sensor_index) {
        client.sensor_bus->reset();
        client.sensor_bus->select(client.addresses[sensor_index]);
        client.sensor_bus->write(0xBE);

        int temperature = client.sensor_bus->read();
        temperature |= client.sensor_bus->read() << 8;
        uint8_t id = client.sensor_bus->read();

#if DEBUG
        Serial.print("0x");
        Serial.print(id, HEX);
        Serial.print(" ");
        Serial.print(temperature * 0.0625f);
        Serial.print(", ");
#endif

        client.payload.sensor_readings[id] = temperature * 0.0625f;
    }

    disable_sensors();
}

void setup() {
    Serial.begin(115200);

    pinMode(STATUS_LED_PIN, OUTPUT);
    digitalWrite(STATUS_LED_PIN, HIGH);

    if (!is_housahedron()) {
        init_node();
    }

    int version = get_version();

    if (version != (HOUSAHEDRON_VERSION_HIGH << 8 | HOUSAHEDRON_VERSION_LOW)) {
        init_node();
    }

    version = get_version();

    uint8_t uuid[16];
    uint8_t node_id = 0;

    get_UUID(uuid);

    char uuid_str[37];
    print_UUID(uuid, uuid_str);
    Serial.print("Sadie v");
    Serial.println(version);
    Serial.println(uuid_str);

    for (byte i = 0; i < 16; ++i) {
        node_id ^= uuid[i];
    }

    node_id &= RF12_HDR_MASK;
    if (node_id == 0 || node_id == 31) {
        node_id = 1;
    }

    rf12_initialize(node_id, RF12_868MHZ, RF_CHANNEL);

    // Switch on all potential sensor buses.
    for (byte bus_num = 0; bus_num < 4; ++bus_num) {
        pinMode(sensor_control_pins[bus_num], OUTPUT);
        digitalWrite(sensor_control_pins[bus_num], HIGH);
    }

    while (client.sensor_bus == 0) {
        byte bus_num = 0;
//        for (byte bus_num = 0; bus_num < 4; ++bus_num) {
            OneWire the_bus(JEENODE_PORT_1 + bus_num);

            for (client.num_sensors = 0; client.num_sensors < MAX_SENSORS; ++client.num_sensors) {
                if (!the_bus.search(client.addresses[client.num_sensors])) {
                    the_bus.reset_search();
                    break;
                }
            }

            if (client.num_sensors > 0) {
                client.sensor_bus = new OneWire(JEENODE_PORT_1 + bus_num);
            }

            if (client.sensor_bus != 0) {
                client.sensor_control_pin = sensor_control_pins[bus_num];
                break;
            }
//        }
    }

    // Now we know which bus the sensors are on, we can switch the rest off.
    for (byte bus_num = 0; bus_num < 4; ++bus_num) {
        digitalWrite(sensor_control_pins[bus_num], LOW);
    }

    digitalWrite(STATUS_LED_PIN, LOW);
    enable_sensors();

    for (byte sensor_index = 0; sensor_index < client.num_sensors; ++sensor_index) {
        client.sensor_bus->reset();
        client.sensor_bus->select(client.addresses[sensor_index]);
        client.sensor_bus->write(0xBE);

        byte scratchpad[5];

        for (byte i = 0; i < 5; ++i) {
            scratchpad[i] = client.sensor_bus->read();
        }

        // Find the highest-precision (i.e. slowest ADC) sensor on the bus.
        byte foo = (scratchpad[4] & 0x60) >> 5;
        client.precision = foo > client.precision ? foo : client.precision;
    }

    client.sensor_bus->reset();

    disable_sensors();

    // If something went wrong with the precision selection, choose the slowest.
    if (client.precision == -1) {
        client.precision = 3;
    }
}

bool wait_for_data() {
    uint32_t target_millis = millis() + RESPONSE_TIMEOUT;
    bool timed_out = false;

    while (!rf12_recvDone() && !timed_out) {
        if (millis() >= target_millis) {
            timed_out = true;
        }
    }

    return (!timed_out && rf12_crc == 0);
}

int main() {
    init();

    setup();
    rf12_sleep(RF12_SLEEP);

    uint8_t uuid[16];
    get_UUID(uuid);

    char uuid_string[37];
    print_UUID(uuid, uuid_string);

    // 23 is the longest allowable client id.
    snprintf(client_id, 23, "Sadie-%s", &uuid_string[24]);

    char b64_uuid[32];
    base64url_encode(b64_uuid, uuid, 16);
    snprintf(temperature_topic, 60, "AMON/temperatureAir/<HB%df/%s", client.num_sensors, b64_uuid);
    snprintf(battery_low_topic, 60, "AMON/status/<HBB/%s", b64_uuid);

#if DEBUG
    Serial.println(client_id);
    Serial.println(temperature_topic);
#endif

    bool running = true;

    digitalWrite(STATUS_LED_PIN, HIGH);

    while (running) {
        rf12_recvDone();

		if (serialEventRun) {
		    serialEventRun();
		}

        if (!client.wait_for_response()) {
            switch (client.state) {
            case SadieMQTTSN::REGISTERING:
            {
#if DEBUG
                Serial.println("REGISTERING");
                delay(100);
#endif
                uint8_t index;
                uint16_t topic_id = client.find_topic_id(temperature_topic, index);

                if (topic_id == 0 || topic_id == 0xffff) {
                    client.register_topic(temperature_topic);
                } else {
                    topic_id = client.find_topic_id(battery_low_topic, index);

                    if (topic_id == 0 || topic_id == 0xffff) {
                        client.register_topic(battery_low_topic);
                    }
                }
                break;
            }

            case SadieMQTTSN::PUBLISHING:
            {
#if DEBUG
                Serial.println("PUBLISHING");
                delay(100);
#endif
                digitalWrite(STATUS_LED_PIN, HIGH);
                uint8_t index;
                uint16_t topic_id = 0;

                if (bat_status == 0) {
                    topic_id = client.find_topic_id(temperature_topic, index);

                    if (topic_id != 0 && topic_id != 0xffff) {
                        take_readings();
                        client.publish(FLAG_QOS_0, topic_id, &client.payload, 3 + (client.num_sensors * sizeof(float)));
                    } else {
                        Serial.println("Unknown topic");
                    }

                    if (initial_samples > 0) {
                        --initial_samples;
                    } else {
                        sleep_time = SLEEP_TIME;
                    }
                } else {
                    sleep_time = BATTERY_LOW_SLEEP_TIME;
                    topic_id = client.find_topic_id(battery_low_topic, index);

                    if (topic_id != 0 && topic_id != 0xffff) {
                        client.publish(FLAG_QOS_0, topic_id, &bat_status, sizeof(char));
                    }
                }

                digitalWrite(STATUS_LED_PIN, LOW);
                client.state = SadieMQTTSN::SLEEPING;
                break;
            }

            case SadieMQTTSN::SLEEPING:
#if DEBUG
                Serial.println("SLEEPING");
                delay(100);
#endif
                low_power(sleep_time, 5);

                rf12_sleep(RF12_WAKEUP);
                bat_status = rf12_lowbat();
//                client.pingreq(client_id);
                client.state = SadieMQTTSN::PUBLISHING;
                break;

            default:
                break;
            }
        } else {
            if (wait_for_data()) {
                client.parse_rf12();
            } else {
                client.timeout();
            }
        }
    }

    return 0;
}
