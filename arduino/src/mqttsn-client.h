/*
mqttsn-client.h
Copyright (C) 2013 Housahedron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __MQTTSN_CLIENT_H__
#define __MQTTSN_CLIENT_H__

#include "mqttsn-messages.h"

class OneWire;

class SadieMQTTSN : public MQTTSN {
public:
    SadieMQTTSN() : MQTTSN(), state(REGISTERING), sensor_bus(NULL), num_sensors(0), battery_status(0),
                    precision(-1), sensor_control_pin(A0), gateway_id(0)
    {
    }

    enum context_states {
        REGISTERING,
        PUBLISHING,
        SLEEPING
    };

    struct sensor_payload {
        uint16_t aggregated;
        uint8_t timestamp;
        float sensor_readings[MAX_SENSORS];
    };

    context_states state;

    OneWire* sensor_bus;
    uint8_t addresses[MAX_SENSORS][8];
    uint8_t num_sensors;

    uint8_t battery_status;
    int8_t precision;
    uint8_t sensor_control_pin;
    uint8_t gateway_id;

    sensor_payload payload;

    virtual void timeout();

protected:
    virtual void regack_handler(const msg_regack* msg);
//    virtual void pingresp_handler();
};


#endif