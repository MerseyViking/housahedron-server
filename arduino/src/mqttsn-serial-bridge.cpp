/*
mqttsn-serial-bridge.cpp
Copyright (C) 2013 Housahedron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>

#include "housahedron_arduino.h"
#include "mqttsn.h"
#include "mqttsn-messages.h"

#include <JeeLib.h>
#include <Ports.h>

#define ADVERTISE_INTERVAL 10

uint8_t response_buffer[RF12_MAXDATA];
msg_advertise adv;


MQTTSN client = MQTTSN();


void serialEvent() {
    if (Serial.available() > 0) {
        uint8_t* response = response_buffer;
        uint8_t packet_length = (uint8_t)Serial.read();
        uint8_t plength_temp = packet_length;
        *response++ = plength_temp--;

        while (plength_temp > 0) {
            while (Serial.available() > 0) {
                *response++ = (uint8_t)Serial.read();
                --plength_temp;
            }
        }

        while (!rf12_canSend()) {
            rf12_recvDone();
        }

        rf12_sendStart(0, response_buffer, packet_length);
        rf12_sendWait(2);
    }
}

void setup() {
    Serial.begin(115200);

    if (!is_housahedron()) {
        init_node();
    }

    int version = get_version();

    if (version != (HOUSAHEDRON_VERSION_HIGH << 8 | HOUSAHEDRON_VERSION_LOW)) {
        init_node();
    }

    uint8_t uuid[16];
    uint8_t node_id = 1;

    get_UUID(uuid);

    for (byte i = 0; i < 16; ++i) {
        node_id ^= uuid[i];
    }

    node_id &= RF12_HDR_MASK;
    if (node_id == 0 || node_id == 31) {
        node_id = 1;
    }

    adv.length = sizeof(msg_advertise);
    adv.type = ADVERTISE;
    adv.gw_id = node_id;
    adv.duration = ADVERTISE_INTERVAL;

    rf12_initialize(node_id, RF12_868MHZ, RF_CHANNEL);
}

int main() {
    init();

    setup();

    rf12_sleep(RF12_WAKEUP);

    client.connect(0, 0, "Phyllis");

    bool running = true;

    while (running) {
		if (serialEventRun) {
		    serialEventRun();
		}

        if (!client.wait_for_response() && rf12_recvDone()) {
            if (rf12_crc == 0) {
                Serial.write((const uint8_t*)rf12_data, rf12_data[0]);
                Serial.flush();
            }
        }
    }

    rf12_sleep(RF12_SLEEP);

    return 0;
}
