/*
housahedron_arduino.cpp
Copyright (C) 2013 Housahedron

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>

#include "housahedron_arduino.h"

#include <EEPROM.h>
#include <JeeLib.h>

// Flags for the random number generator.
volatile unsigned char sample_waiting = false;
volatile unsigned char sample = 0;

void generate_UUID(unsigned char* uuid) {
    if (uuid == 0) {
        return;
    }

    cli();
    TCCR1A = 0;
    TCCR1B = 0;
    TCNT1 = 0;
    OCR1A = 30011;
    // turn on CTC mode
    TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler
    TCCR1B |= (1 << CS12) | (1 << CS10);

    MCUSR = 0;

    //* Start timed sequence
    WDTCSR |= _BV(WDCE) | _BV(WDE);

    //* Put WDT into interrupt mode
    //* Set shortest prescaler(time-out) value = 2048 cycles (~16 ms)
    WDTCSR = _BV(WDIE);

    sei();

    for (unsigned char i = 0; i < 16; ++i) {
        unsigned char octet = 0;

        for (unsigned char b = 0; b < 8; ++b) {
            while (!sample_waiting) {;}

            sample_waiting = false;

            octet <<= 1;
            octet ^= sample;
        }

        uuid[i] = octet;
    }

    // We're generating Version 4 UUIDs, so we need to set a few bits to known values.
    uuid[6] = (uuid[6] & 0x0f) | 0x40;
    uuid[8] = (uuid[8] & 0x0f) | 0x80; // Can be one of 8, 9, a, or b.
}

// Interrupt service routine for the random number generator's watchdog timer. Also used for the JeeNode's low-power delay functions.
ISR(WDT_vect) {
  sample = TCNT1L;
  sample_waiting = true;
  Sleepy::watchdogEvent();
}

void print_UUID(unsigned char* uuid, char* uuid_string) {
    sprintf_P(uuid_string, PSTR("%02x%02x%02x%02x-%02x%02x-%02x%02x-%02x%02x-%02x%02x%02x%02x%02x%02x"),
    uuid[0], uuid[1], uuid[2], uuid[3],
    uuid[4], uuid[5],
    uuid[6], uuid[7],
    uuid[8], uuid[9],
    uuid[10], uuid[11], uuid[12], uuid[13], uuid[14], uuid[15]
    );
}

bool is_housahedron() {
    unsigned int magic = ((int)EEPROM.read(EEPROM_MAGIC_OFFSET) << 8) | (int)(EEPROM.read(EEPROM_MAGIC_OFFSET + 1));

    return magic == HOUSAHEDRON_MAGIC;
}

int get_version() {
    return (EEPROM.read(EEPROM_VERSION_OFFSET) << 8) | (EEPROM.read(EEPROM_VERSION_OFFSET + 1));
}

void get_UUID(unsigned char* uuid) {
    if (uuid == 0) {
        return;
    }

    for (unsigned char i = 0; i < 16; ++i) {
        uuid[i] = EEPROM.read(EEPROM_UUID_OFFSET + i);
    }
}

// Stores the magic number, version, and UUID in the EEPROM.
void init_node() {
    EEPROM.write(EEPROM_MAGIC_OFFSET, (HOUSAHEDRON_MAGIC >> 8) & 0xff);
    EEPROM.write(EEPROM_MAGIC_OFFSET + 1, HOUSAHEDRON_MAGIC & 0xff);

    EEPROM.write(EEPROM_VERSION_OFFSET, HOUSAHEDRON_VERSION_HIGH);
    EEPROM.write(EEPROM_VERSION_OFFSET + 1, HOUSAHEDRON_VERSION_LOW);

    byte uuid[16];
    bool uuid_ok = false;

    for (unsigned char i = 0; i < 16; ++i) {
        byte b = EEPROM.read(EEPROM_UUID_OFFSET + i);

        if (b != 0xff) {
            uuid_ok = true;
            break;
        }
    }

    if (!uuid_ok) {
        generate_UUID(uuid);

        for (unsigned char i = 0; i < 16; ++i) {
            EEPROM.write(EEPROM_UUID_OFFSET + i, uuid[i]);
        }
    }
}

void dump_EEPROM() {
    char buf[8];

    for (int i = 0; i < 1024; ++i) {
        byte b = EEPROM.read(i);
        sprintf(buf, "0x%02X ", b);
        Serial.print(buf);

        if (i % 16 == 15) {
            Serial.println();
        }
    }
}

// Enable the use of fprintf to print to the serial port.
FILE uartout = {0};

static int uart_putchar(char c, FILE* stream) {
    Serial.write(c);
    return 0;
}

void init_uart() {
    fdev_setup_stream(&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);
}

void start_timer(unsigned long& timer) {
    timer = millis();
}

bool check_timer(unsigned long& timer, unsigned long interval) {
    unsigned long cur_tick = millis();
    unsigned long diff = cur_tick - timer;

    // After 49 days 17 hours 2 minutes and 47.295 seconds (approximately), the counter will wrap around.
    if (cur_tick < timer) {
        diff = (0xffffffff - timer) + cur_tick;
    }

    if (diff >= interval) {
        return true;
    }

    return false;
}

// See: http://forum.arduino.cc/index.php/topic,44216.0.html#11
//
// Produce a formatted string in a buffer corresponding to the value provided.
// If the 'width' parameter is non-zero, the value will be padded with leading
// zeroes to achieve the specified width.  The number of characters added to
// the buffer (not including the null termination) is returned.
//
unsigned int fmtUnsigned(unsigned long val, char *buf, unsigned bufLen, byte width) {
  if (!buf || !bufLen)
    return(0);

  // produce the digit string (backwards in the digit buffer)
  char dbuf[10];
  unsigned idx = 0;
  while (idx < sizeof(dbuf))
  {
    dbuf[idx++] = (val % 10) + '0';
    if ((val /= 10) == 0)
      break;
  }

  // copy the optional leading zeroes and digits to the target buffer
  unsigned len = 0;
  byte padding = (width > idx) ? width - idx : 0;
  char c = '0';
  while ((--bufLen > 0) && (idx || padding))
  {
    if (padding)
      padding--;
    else
      c = dbuf[--idx];
    *buf++ = c;
    len++;
  }

  // add the null termination
  *buf = '\0';
  return(len);
}

//
// Format a floating point value with number of decimal places.
// The 'precision' parameter is a number from 0 to 6 indicating the desired decimal places.
// The 'buf' parameter points to a buffer to receive the formatted string.  This must be
// sufficiently large to contain the resulting string.  The buffer's length may be
// optionally specified.  If it is given, the maximum length of the generated string
// will be one less than the specified value.
//
// example: fmtDouble(3.1415, 2, buf); // produces 3.14 (two decimal places)
//
void fmtDouble(double val, byte precision, char *buf, unsigned bufLen) {
  if (!buf || !bufLen)
    return;

  // limit the precision to the maximum allowed value
  const byte maxPrecision = 6;
  if (precision > maxPrecision)
    precision = maxPrecision;

  if (--bufLen > 0)
  {
    // check for a negative value
    if (val < 0.0)
    {
      val = -val;
      *buf = '-';
      bufLen--;
    }

    // compute the rounding factor and fractional multiplier
    double roundingFactor = 0.5;
    unsigned long mult = 1;
    for (byte i = 0; i < precision; i++)
    {
      roundingFactor /= 10.0;
      mult *= 10;
    }

    if (bufLen > 0)
    {
      // apply the rounding factor
      val += roundingFactor;

      // add the integral portion to the buffer
      unsigned len = fmtUnsigned((unsigned long)val, buf, bufLen);
      buf += len;
      bufLen -= len;
    }

    // handle the fractional portion
    if ((precision > 0) && (bufLen > 0))
    {
      *buf++ = '.';
      if (--bufLen > 0)
        buf += fmtUnsigned((unsigned long)((val - (unsigned long)val) * mult), buf, bufLen, precision);
    }
  }

  // null-terminate the string
  *buf = '\0';
}

const char* encoding_table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_";

void base64url_encode(char* output, const uint8_t* input, const uint8_t input_length) {
    uint16_t buf = 0;
    uint8_t remainder = 0;
    uint8_t i = 0;

    while (i < input_length) {
        if (remainder < 6) {
            buf |= input[i] << (8 - remainder);
            remainder += 8;
            i += 1;
        }

        *(output++) = encoding_table[(buf & 0xfc00) >> 10];
        buf <<= 6;
        remainder -= 6;
    }

    output[0] = encoding_table[(buf & 0xfc00) >> 10];

    if (input_length % 3 == 1) {
        output[1] = '=';
        output[2] = '=';
        output[3] = 0;
    } else if (input_length % 3 == 2) {
        output[1] = '=';
        output[2] = 0;
    } else {
        output[1] = 0;
    }
}

uint8_t base64url_encode_length(const uint8_t input_length) {
    return ((input_length + 2) / 3) * 4;
}
