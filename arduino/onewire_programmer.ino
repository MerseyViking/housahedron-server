/*
# onewire_programmer.ino
# Copyright (C) 2013 Housahedron
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EEPROM.h>
#include <JeeLib.h>
#include <Ports.h>
#include <OneWire.h>

#include "housahedron_arduino.h"

byte num_sensors = 0;

OneWire* sensor_bus = 0;

byte addresses[MAX_SENSORS][8];
int readings[MAX_SENSORS];
byte ids[MAX_SENSORS];
byte configs[MAX_SENSORS];
boolean identified[MAX_SENSORS];
static const byte sensor_control_pins[] = {A0, A1, A2, A3};
static byte sensor_control_pin = A0;

long mean = 0;
int num_readings = 0;
const int min_readings_per_sensor = 20;
int identified_count = 0;

byte current_sensor = 0;
boolean wait_message = true;
boolean found_message = true;

void enable_sensors() {
    digitalWrite(sensor_control_pin, HIGH);
    delay(10);
}

void disable_sensors() {
    digitalWrite(sensor_control_pin, LOW);
}

void setup() {
    pinMode(A0, OUTPUT);
    digitalWrite(A0, HIGH);
    delay(10);

    Serial.begin(115200);

    for (byte bus_num = 0; bus_num < 4; ++bus_num) {
        pinMode(sensor_control_pins[bus_num], OUTPUT);
        digitalWrite(sensor_control_pins[bus_num], HIGH);
    }

    while (sensor_bus == 0) {
        for (byte bus_num = 0; bus_num < 4; ++bus_num) {
            OneWire the_bus(JEENODE_PORT_1 + bus_num);

            for (num_sensors = 0; num_sensors < MAX_SENSORS; ++num_sensors) {
                if (!the_bus.search(addresses[num_sensors])) {
                    the_bus.reset_search();
                    break;
                }
            }

            if (num_sensors > 0) {
                sensor_bus = new OneWire(JEENODE_PORT_1 + bus_num);
                Serial.print("Found ");
                Serial.print(num_sensors, DEC);
                Serial.print(" sensors on bus ");
                Serial.println(bus_num + 1, DEC);
            }

            if (sensor_bus != 0) {
                sensor_control_pin = sensor_control_pins[bus_num];
                break;
            }
        }

        if (num_sensors == 0 && found_message) {
                Serial.println("Searching for sensors...");
                found_message = false;
        }

        delay(1000);
    }

    for (byte sensor_id = 0; sensor_id < num_sensors; ++sensor_id) {
        sensor_bus->reset();
        sensor_bus->select(addresses[sensor_id]);
        sensor_bus->write(0xBE);

        // Ignore the temperature values.
        sensor_bus->read();
        sensor_bus->read();

        Serial.print("ID: 0x");
        Serial.print(sensor_bus->read(), HEX);

        // Unused byte.
        sensor_bus->read();

        // Configuration byte.
        byte config = sensor_bus->read();

        Serial.print(" Resolution: ");

        switch (config & 0x60) {
        case 0x60:
            Serial.print("0.0625 deg C (12-bit, 750ms)");
            break;

        case 0x40:
            Serial.print("0.125 deg C (11-bit, 375ms)");
            break;

        case 0x20:
            Serial.print("0.25 deg C (10-bit, 187.5ms)");
            break;

        case 0x00:
            Serial.print("0.5 deg C (9-bit, 93.75ms)");
            break;
        }

        Serial.println();
    }

    digitalWrite(A0, LOW);

    calibrate();
}

void loop() {
    if (wait_message) {
        Serial.print("Waiting for sensor ");
        Serial.println(current_sensor);
        wait_message = false;
    }

    read_sensors();
    enable_sensors();

    for (byte sensor_id = 0; sensor_id < num_sensors; ++sensor_id) {
        if (!identified[sensor_id] && readings[sensor_id] > (mean + 48)) {
            Serial.println("Identified");

            ++identified_count;

            sensor_bus->reset();
            sensor_bus->select(addresses[sensor_id]);
            // Put the sensor number into the Th register, and make sure the other two bytes are defaults.
            sensor_bus->write(0x4E);
            sensor_bus->write(current_sensor);
            sensor_bus->write(0x00);
            // Set the resolution to 10 bits, (0.25 degrees, 187.5ms reading time).
            //sensor_bus->write((configs[sensor_id] & 0x9f) | 0x20);

            // Set the resolution to 12 bits, (0.0625 degrees, 750ms reading time).
            sensor_bus->write((configs[sensor_id] & 0x9f) | 0x60);

            // Put the new data into the sensor's EEPROM.
            sensor_bus->reset();
            sensor_bus->select(addresses[sensor_id]);
            sensor_bus->write(0x48);

            sensor_bus->reset();
            ++current_sensor;
            wait_message = true;
            identified[sensor_id] = true;
            break;
        }
    }

    disable_sensors();

    if (identified_count == num_sensors) {
        while (true) {
            read_sensors();
            for (int i = 0; i < num_sensors; ++i) {
                Serial.print("0x");
                Serial.print(ids[i], HEX);
                Serial.print(" ");
                Serial.print(readings[i] * 0.0625);
                Serial.print("  ");
            }

            Serial.println("");

            delay(1250);
        }
    }
}

void read_sensors() {
    enable_sensors();

    // Tell all sensors on the bus to take a reading.
    sensor_bus->reset();
    sensor_bus->write(0xCC);
    sensor_bus->write(0x44);

    delay(750);

    for (byte sensor_id = 0; sensor_id < num_sensors; ++sensor_id) {
        sensor_bus->reset();
        sensor_bus->select(addresses[sensor_id]);
        sensor_bus->write(0xBE);

        int temp = sensor_bus->read();
        temp |= sensor_bus->read() << 8;

        readings[sensor_id] = temp;
        ids[sensor_id] = sensor_bus->read();
        sensor_bus->read();

        configs[sensor_id] = sensor_bus->read();
    }

    disable_sensors();
}

void calibrate() {
    Serial.println("Determining mean temperature...");

    while (num_readings < min_readings_per_sensor * num_sensors) {
        read_sensors();

        for (byte sensor_id = 0; sensor_id < num_sensors; ++sensor_id) {
            mean += readings[sensor_id];
            ++num_readings;
        }
    }

    mean /= num_readings;
    Serial.print("Taken ");
    Serial.print(num_readings);
    Serial.print(" readings, with a mean temperature of ");
    Serial.println(mean * 0.0625);
}