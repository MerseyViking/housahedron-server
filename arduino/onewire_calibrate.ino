/*
# onewire_calibrate.ino
# Copyright (C) 2013 Housahedron
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EEPROM.h>
#include <JeeLib.h>
#include <Ports.h>
#include <OneWire.h>

#include "housahedron_arduino.h"

#define HISTORY_SIZE 32

byte num_sensors = 0;

OneWire sensor_bus(JEENODE_PORT_1);
OneWire control_bus(JEENODE_PORT_2);
byte addresses[MAX_SENSORS][8];
byte control_address[8] = {0, 0, 0, 0, 0, 0, 0, 0};

int readings[MAX_SENSORS];
byte ids[MAX_SENSORS];
char offset_history[MAX_SENSORS][HISTORY_SIZE];
char mean_offset[MAX_SENSORS];
byte cur_reading = 0;
bool history_full = false;
int control_temp;

long mean = 0;
int num_readings = 0;
const int min_readings_per_sensor = 20;
int identified_count = 0;

byte current_sensor = 0;
boolean wait_message = true;

void setup() {
    pinMode(A0, OUTPUT);

    memset(offset_history, 0, MAX_SENSORS * HISTORY_SIZE * sizeof(char));
    memset(mean_offset, 0, MAX_SENSORS * sizeof(char));

    Serial.begin(115200);

    if (!control_bus.search(control_address)) {
        Serial.println();
        Serial.println("Unable to find control sensor.");
        while(true) {}
    }

    control_bus.reset_search();

    digitalWrite(A0, HIGH);
    delay(10);

    for (num_sensors = 0; num_sensors < MAX_SENSORS; ++num_sensors) {

        if (!sensor_bus.search(addresses[num_sensors])) {
            sensor_bus.reset_search();
            break;
        }
    }

    Serial.print("Found ");
    Serial.print(num_sensors);
    Serial.println(" sensors.");

    for (byte sensor_id = 0; sensor_id < num_sensors; ++sensor_id) {
        sensor_bus.reset();
        sensor_bus.select(addresses[sensor_id]);
        sensor_bus.write(0xBE);

        // Ignore the temperature values.
        sensor_bus.read();
        sensor_bus.read();

        Serial.print("ID: 0x");
        ids[sensor_id] = sensor_bus.read();
        Serial.print(ids[sensor_id], HEX);

        // Unused byte.
        sensor_bus.read();

        // Configuration byte.
        byte config = sensor_bus.read();

        Serial.print(" Resolution: ");

        switch (config & 0x60) {
        case 0x60:
            Serial.print("0.0625 deg C (12-bit, 750ms)");
            break;

        case 0x40:
            Serial.print("0.125 deg C (11-bit, 375ms)");
            break;

        case 0x20:
            Serial.print("0.25 deg C (10-bit, 187.5ms)");
            break;

        case 0x00:
            Serial.print("0.5 deg C (9-bit, 93.75ms)");
            break;
        }

        Serial.println();
    }

//    digitalWrite(A0, LOW);

    // Simple bubble-sort to put the sensors in order.
    bool sorted = false;

    while(!sorted) {
        sorted = true;

        for (byte i = 0; i < num_sensors - 1; ++i) {
            if (ids[i] > ids[i + 1]) {
                sorted = false;

                byte temp_id = ids[i];
                ids[i] = ids[i + 1];
                ids[i + 1] = temp_id;

                byte temp_address[8];
                memcpy(temp_address, addresses[i], 8);
                memcpy(addresses[i], addresses[i + 1], 8);
                memcpy(addresses[i + 1], temp_address, 8);
            }
        }
    }

//    calibrate();
}

void loop() {
    read_sensors();

    Serial.print("Control: ");
    Serial.println(control_temp * 0.0625);

    for (byte i = 0; i < num_sensors; ++i) {
        Serial.print("ID: 0x");
        Serial.print(ids[i], HEX);
        Serial.print(" ");
        Serial.print(readings[i] * 0.0625);

        offset_history[i][cur_reading] = control_temp - readings[i];

        if (history_full) {
            int sum = 0;
            for (byte j = 0; j < HISTORY_SIZE; ++j) {
                sum += offset_history[i][j];
            }

            mean_offset[i] = sum / HISTORY_SIZE;
            Serial.print(" (");
            Serial.print(mean_offset[i], DEC);
            Serial.print(") ");
            Serial.print((readings[i] + mean_offset[i]) * 0.0625);
        }

        Serial.println();

    }

    Serial.println();

    cur_reading = (cur_reading + 1) % HISTORY_SIZE;

    if (cur_reading == 0) {
        history_full = true;
    }

    delay(3500);
}

void read_sensors() {
//    digitalWrite(A0, HIGH);
//    delay(10);

    // Tell all sensors on the bus to take a reading.
    sensor_bus.reset();
    sensor_bus.write(0xCC);
    sensor_bus.write(0x44);

    control_bus.reset();
    control_bus.write(0xCC);
    control_bus.write(0x44);

    delay(750);

    control_bus.reset();
    control_bus.select(control_address);
    control_bus.write(0xBE);

    control_temp = control_bus.read();
    control_temp |= control_bus.read() << 8;

    for (byte sensor_id = 0; sensor_id < num_sensors; ++sensor_id) {
        sensor_bus.reset();
        sensor_bus.select(addresses[sensor_id]);
        sensor_bus.write(0xBE);

        int temp = sensor_bus.read();
        temp |= sensor_bus.read() << 8;

        readings[sensor_id] = temp;
        ids[sensor_id] = sensor_bus.read();
    }

//    digitalWrite(A0, LOW);
}

void calibrate() {
  Serial.println("Determining mean temperature...");
  while (num_readings < min_readings_per_sensor * num_sensors) {
    read_sensors();

    for (byte sensor_id = 0; sensor_id < num_sensors; ++sensor_id) {
      mean += readings[sensor_id];
      ++num_readings;
    }
  }

  mean /= num_readings;
  Serial.print("Taken ");
  Serial.print(num_readings);
  Serial.print(" readings, with a mean temperature of ");
  Serial.println(mean * 0.0625);
}