/*
# sender_1wire.ino
# Copyright (C) 2013 Housahedron
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EEPROM.h>
#include <JeeLib.h>
#include <Ports.h>
#include <OneWire.h>

#include "housahedron_arduino.h"

#define DEBUG 0
#define DEMO 0

#define REREGISTER_TIMEOUT 12
// The maximum for loseSomeTime() is 65535
// On the JeeNodes that have been tested, loseSomeTime() seems to add an extra second per minute, so this compensates
// for that which gives us a slightly more accurate 5 minute delay.
#define DEMO_SLEEP_TIME 1300
#define SLEEP_TIME 59000

// For the DS18B20, the time between requesting a temperature and getting it depends on the precision of the ADC.
static const int ADC_delay[] = {94, 188, 375, 750};
int precision = -1;

static const byte sensor_control_pins[] = {A0, A1, A2, A3};
static byte sensor_control_pin = A0;

// Eventually we'll have more than one bus.
//OneWire buses[4] = {OneWire(JEENODE_PORT_1), OneWire(JEENODE_PORT_2), OneWire(JEENODE_PORT_3), OneWire(JEENODE_PORT_4)};
OneWire* sensor_bus = 0;
static byte addresses[MAX_SENSORS][8];
static byte num_sensors = 0;
static byte uuid[16];
static byte node_id = 0;
// Every REREGISTER_TIMEOUT * SLEEP_TIME milliseconds, broadcast our presence again.
static byte reregister_timer = 0;

static byte battery_status = 0;

payload_packet* payload_buffer = 0;

void enable_sensors() {
    digitalWrite(sensor_control_pin, HIGH);
    delay(10);
}

void disable_sensors() {
    digitalWrite(sensor_control_pin, LOW);
}

// Registers this node with one or more base stations.
// Eventually we'll need to address the issue of accidentally registering this node with a neighbour's base station.
void register_node() {
    node_metadata* metadata = (node_metadata*)payload_buffer->data;
    metadata->num_sensors = num_sensors;

    for (byte i = 0; i < num_sensors; ++i) {
        metadata->sensor_types[i] = TYPE_TEMPERATURE;
    }

    metadata->battery_status = battery_status;

    rf12_sleep(RF12_WAKEUP);
    while (!rf12_canSend()) {
        rf12_recvDone();
        Sleepy::loseSomeTime(32);
    }

    payload_buffer->command = CMD_REGISTER;
    memcpy(payload_buffer->uuid, uuid, 16);
    rf12_sendStart(0, payload_buffer, sizeof(payload_packet) + sizeof(node_metadata));
    rf12_sendWait(2);

    // TODO: Wait for an ACK from the base station.

    rf12_sleep(RF12_SLEEP);
    delay(10);
}

void setup() {
    delay(1000);
    Serial.begin(115200);
    init_uart();

    payload_buffer = (payload_packet*)malloc(66);

    if (!is_housahedron()) {
        fprintf_P(&uartout, PSTR("New Jeenode detected. Initializing in 5 seconds...\n"));
        delay(5000);
        init_node();
        fprintf_P(&uartout, PSTR("Jeenode initialized.\n"));
    }

    int version = get_version();

    if (version != (HOUSAHEDRON_VERSION_HIGH << 8 | HOUSAHEDRON_VERSION_LOW)) {
        init_node();
    }

    version = get_version();

    fprintf_P(&uartout, PSTR("Housahedron Phillis v%d.%d\n\n"), (byte)(version >> 8), (byte)version);

    get_UUID(uuid);

    char uuid_string[37];
    print_UUID(uuid, uuid_string);
    fprintf_P(&uartout, PSTR("UUID: %s\n"), uuid_string);

    for (byte i = 0; i < 16; ++i) {
        node_id ^= uuid[i];
    }

    node_id &= RF12_HDR_MASK;
    if (node_id == 0 || node_id == 31) {
        node_id = 1;
    }

    rf12_initialize(node_id, RF12_868MHZ, RF_CHANNEL);

    fprintf_P(&uartout, PSTR("RFID: 0x%02X Freq: %dMHz Channel: 0x%02X\n"), node_id, 868, RF_CHANNEL);
    fprintf_P(&uartout, PSTR("Searching for OneWire bus...\n"));

    // Switch on all potential sensor buses.
    for (byte bus_num = 0; bus_num < 4; ++bus_num) {
        pinMode(sensor_control_pins[bus_num], OUTPUT);
        digitalWrite(sensor_control_pins[bus_num], HIGH);
    }

    while (sensor_bus == 0) {
        for (byte bus_num = 0; bus_num < 4; ++bus_num) {
            OneWire the_bus(JEENODE_PORT_1 + bus_num);

            for (num_sensors = 0; num_sensors < MAX_SENSORS; ++num_sensors) {
                if (!the_bus.search(addresses[num_sensors])) {
                    the_bus.reset_search();
                    break;
                }
            }

            if (num_sensors > 0) {
                sensor_bus = new OneWire(JEENODE_PORT_1 + bus_num);
                fprintf_P(&uartout, PSTR("Found %d sensors on bus %d\n"), num_sensors, bus_num + 1);
            }

            if (sensor_bus != 0) {
                sensor_control_pin = sensor_control_pins[bus_num];
                break;
            }
        }
    }

    // Now we know which bus the sensors are on, we can switch the rest off.
    for (byte bus_num = 0; bus_num < 4; ++bus_num) {
        digitalWrite(sensor_control_pins[bus_num], LOW);
    }

    enable_sensors();

    for (byte sensor_index = 0; sensor_index < num_sensors; ++sensor_index) {
        sensor_bus->reset();
        sensor_bus->select(addresses[sensor_index]);
        sensor_bus->write(0xBE);

        byte scratchpad[5];

        for (byte i = 0; i < 5; ++i) {
            scratchpad[i] = sensor_bus->read();
        }

        // Find the highest-precision (i.e. slowest ADC) sensor on the bus.
        byte foo = (scratchpad[4] & 0x60) >> 5;
        precision = foo > precision ? foo : precision;
    }

    sensor_bus->reset();

    disable_sensors();

    // If something went wrong with the precision selection, choose the slowest.
    if (precision == -1) {
        precision = 3;
    }

    fprintf_P(&uartout, PSTR("Sensors: %d\nReady.\n"), num_sensors);
    delay(10);

    register_node();
}

void loop() {
#if DEBUG
    Serial.print("Taking reading ");
    delay(1);
#endif

    enable_sensors();

    sensor_bus->reset();
    sensor_bus->write(0xCC);
    sensor_bus->write(0x44);

    Sleepy::loseSomeTime(ADC_delay[precision]);

    sensor_payload* sensor_readings = (sensor_payload*)payload_buffer->data;

    for (byte sensor_index = 0; sensor_index < num_sensors; ++sensor_index) {
        sensor_bus->reset();
        sensor_bus->select(addresses[sensor_index]);
        sensor_bus->write(0xBE);

        int temperature = sensor_bus->read();
        temperature |= sensor_bus->read() << 8;
        byte id = sensor_bus->read();

#if DEBUG
        Serial.print("0x");
        Serial.print(id, HEX);
        Serial.print(" ");
        Serial.print(temperature * 0.0625);
        Serial.print(", ");
#endif
        sensor_readings[sensor_index].sensor_id = id;
        sensor_readings[sensor_index].sensor_type = TYPE_TEMPERATURE;
        sensor_readings[sensor_index].sensor_value = temperature * 0.0625f;
    }

    disable_sensors();

    /*
    TODO: Because the RF12's buffer is only 66 bytes long, we should break the readings into blocks of eight, and send
          them one after the other.
    */

    // TODO: Only send packets if there is a suitably large delta from the previous readings.

    // TODO: We should wait for an ACK from Phyllis, and any other data it might want to send - for instance changing
    // the delay between readings.
    rf12_sleep(RF12_WAKEUP);
    while (!rf12_canSend()) {
        rf12_recvDone();
    }

    battery_status = rf12_lowbat();

    payload_buffer->command = CMD_SEND_READING;
    rf12_sendStart(0, payload_buffer, sizeof(payload_packet) + (num_sensors * sizeof(sensor_payload)));

    rf12_sendWait(2);
    rf12_sleep(RF12_SLEEP);

#if DEBUG
    Serial.println("OK");
    delay(1);
#endif

#if DEMO
    Sleepy::loseSomeTime(DEMO_SLEEP_TIME);
#else
    Sleepy::loseSomeTime(SLEEP_TIME);
    Sleepy::loseSomeTime(SLEEP_TIME);
    Sleepy::loseSomeTime(SLEEP_TIME);
    Sleepy::loseSomeTime(SLEEP_TIME);
    Sleepy::loseSomeTime(SLEEP_TIME);
#endif

    ++reregister_timer;

    if (reregister_timer >= REREGISTER_TIMEOUT) {
        reregister_timer = 0;
        register_node();
    }

    // TODO: Reregister when sensors have been removed or added.
}
