/*
# receiver_jeenode.ino
# Copyright (C) 2013 Housahedron
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EEPROM.h>
#include <JeeLib.h>
#include <Ports.h>

#include "housahedron_arduino.h"

#define MAX_NODES 30

struct node_record {
    unsigned char node_id[16];
    node_metadata metadata;
};

static node_record node_table[MAX_NODES];
static byte uuid[16];
static char uuid_string[37];
static byte node_id = 0;
static byte node_count = 0;

const char device_string[] PROGMEM = "{"
"\"deviceId\":\"%s\","
"\"meteringPointId\":\"%s\","
"\"privacy\":\"private\","
"\"readings\":[{"
"\"type\":\"%s\","
"\"period\":\"INSTANT\","
"\"unit\":\"%s\""
"}],"
"\"measurements\":[{"
"\"type\":\"%s\","
"\"value\":%s"
"}]"
"}";

const char metering_points_string[] PROGMEM = "{\"meteringPointId\":\"%s\",\"entityId\":\"%s\",\"metadata\":{\"battery_status\":%d}}";

const char entity_string[] PROGMEM = "{\"entities\":[{\"entityId\":\"%s\"}]}";

void setup() {
    Serial.begin(115200);
    init_uart();

    if (!is_housahedron()) {
        init_node();
    }

    int version = get_version();

    if (version != (HOUSAHEDRON_VERSION_HIGH << 8 | HOUSAHEDRON_VERSION_LOW)) {
        init_node();
    }

    version = get_version();

    serial_log_P(PSTR("Housahedron Sadie v%d.%d"), (byte)(version >> 8), (byte)version);

    get_UUID(uuid);

    print_UUID(uuid, uuid_string);
    serial_log_P(PSTR("UUID: %s"), uuid_string);

    for (byte i = 0; i < 16; ++i) {
        node_id ^= uuid[i];
    }

    node_id &= RF12_HDR_MASK;

    rf12_initialize(node_id, RF12_868MHZ, RF_CHANNEL);

    serial_log_P(PSTR("RFID: 0x%02X Freq: %dMHz Channel: 0x%02X"), node_id, 868, RF_CHANNEL);

    serial_log_P(PSTR("Ready."));

    serial_data_P(entity_string, uuid_string);
    Serial.flush();
}

void loop() {
    if (rf12_recvDone() && rf12_crc == 0) {
        payload_packet* packet = (payload_packet*)rf12_data;

        if (packet->command == CMD_REGISTER) {
            byte idx = index_from_node(packet->uuid);

            if (idx == 0xff) {
                memcpy(node_table[node_count].node_id, packet->uuid, 16);
                idx = node_count;
                ++node_count;
            }

            memcpy(&node_table[idx].metadata, packet->data, sizeof(node_metadata));

            // Update the server with all the nodes.
            fwrite(&SERIAL_START_DATA, 1, 1, &uartout);
            fprintf_P(&uartout, PSTR("{\"meteringPoints\": ["));

            for (byte i = 0; i < node_count; ++i) {
                char id_string[37];
                print_UUID(node_table[i].node_id, id_string);

                fprintf_P(&uartout, metering_points_string, id_string, uuid_string, node_table[i].metadata.battery_status);

                if (i < node_count - 1) {
                    fprintf_P(&uartout, PSTR(","));
                }
            }
            fprintf_P(&uartout, PSTR("]}"));
            fwrite(&SERIAL_STOP, 1, 1, &uartout);
            Serial.flush();
        } else if (packet->command == CMD_SEND_READING) {

            char id_string[37];
            print_UUID(packet->uuid, id_string);
            // TODO: Because the RF12's buffer is only 66 bytes long, we may need some sort of continuation command
            // to allow us to have more than a handful of sensors per node.

            sensor_payload* sensor_readings = (sensor_payload*)packet->data;
            byte idx = index_from_node(packet->uuid);

            if (idx != 0xff) {
                char float_buff[16];
                // To identify an individual sensor, we take the node's UUID and change the '8' to a '9' in the first
                // nibble of the 4th group, and the last byte to the sensor id. The assumption is that the rest of the
                // UUID is sufficiently unique within the domain of nodes for one byte not to affect it. Changing the
                // '8' guarantees there are no collisions with the node's UUID. This isn't strictly necessary because
                // the UUIDs only need to be unique within their own domain.
                unsigned char sensor_uuid[16];
                memcpy(sensor_uuid, node_table[idx].node_id, 16);
                sensor_uuid[8] = (sensor_uuid[8] & 0x0F) | 0x90;

                char sensor_uuid_string[37];

                fwrite(&SERIAL_START_DATA, 1, 1, &uartout);
                fprintf_P(&uartout, PSTR("{\"devices\": ["));

                for (byte i = 0; i < node_table[idx].metadata.num_sensors; ++i) {
                    fmtDouble(sensor_readings[i].sensor_value, 2, float_buff, 16);
                    sensor_uuid[15] = i;
                    print_UUID(sensor_uuid, sensor_uuid_string);

                    fprintf_P(&uartout, device_string, sensor_uuid_string, id_string, "temperatureAir", "C", "temperatureAir", float_buff);

                    if (i < node_table[idx].metadata.num_sensors - 1) {
                        fprintf_P(&uartout, PSTR(","));
                    }
                }
                fprintf_P(&uartout, PSTR("]}"));
                fwrite(&SERIAL_STOP, 1, 1, &uartout);
                Serial.flush();
            } else {
                // TODO: We should send a ACK packet back, and if the node is unknown, tell the remote node to register itself.
            }
        }
    }
}

byte index_from_node(byte* uuid) {
    for (byte i = 0; i < node_count; ++i) {
        if (memcmp(node_table[i].node_id, uuid, 16) == 0) {
            return i;
        }
    }

    return 0xff;
}
