import os, re
import Image, ImageDraw
import pylas

lasfile = "../ZEB1/Office/Housahedron_01.las"
#lasfile = "../ZEB1/Cave/Scan1/charlie1.las"
#lasfile = "../ZEB1/Cave/Scan2/02_creswell.las"

renddir = "../renderedlayers"
for f in os.listdir(renddir):
    if re.search("\.png$(?i)", f):
        lf = os.path.join(renddir, f)
        os.remove(lf)

    
def Main():
    verts = pylas.ReadLAS(lasfile, 0)
    
    def middle99(vs):
        vs.sort()
        v5 = int(min(0.005*len(vs), len(vs)/2 - 100))
        v95 = len(vs) - 1 - v5
        w = vs[v95] - vs[v5]
        return vs[v5] - w*0.05, vs[v95] + w*0.05
    xmin, xmax = middle99([v[0]  for v in verts])
    print xmin, xmax
    ymin, ymax = middle99([v[1]  for v in verts])
    print ymin, ymax
    zmin, zmax = middle99([v[2]  for v in verts])
    print zmin, zmax
    nzlayers = 50
    dim = (1200, 1200)   # W, H
    d = (zmax - zmin) / nzlayers
    sca = min(dim[0] / (xmax - xmin), dim[1] / (ymax - ymin))*0.95
    xoffs = dim[0] / 2 - (xmax + xmin) / 2 * sca
    yoffs = dim[1] / 2 - (ymax + ymin) / 2 * sca
    ymoffs = dim[1] / 2 + (ymax + ymin) / 2 * sca
    for i in range(0, nzlayers):
        z = zmin + (zmax - zmin) * (i + 0.5) / nzlayers
        img = MakeImage(verts, z, d, dim, sca, xoffs, ymoffs)
        fname = os.path.join(renddir, "img%03d.png" % i)
        img.save(fname, "PNG")


def MakeImage(verts, z, d, dim, sca, xoffs, ymoffs):
    img = Image.new("RGBA", dim)  # transparent
    draw = ImageDraw.Draw(img)
    lv = [ v  for v in verts  if z-d <= v[2] <= z+d ]
    print z, len(lv)
    lv.sort(key=lambda v: -abs(v[2]-z))
    for v in lv:
        x = int(v[0] * sca + xoffs)
        y = int(ymoffs - v[1] * sca)
        a = (v[2] - z)/d
        b = 1-a*a
        if a < 0:
            c = (int(255*b),int((a+1)*b*255),0)
        else:
            c = (int((1-a)*b*255),int(255*b),0)
        draw.point((x, y), fill=c)
    draw.text((2, 2), "z=%f" % z)
    return img

Main()

