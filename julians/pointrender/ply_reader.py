import struct

type_dict = {'char': 'c',
             'uchar': 'B',
             'short': 'h',
             'ushort': 'H',
             'int': 'i',
             'uint': 'I',
             'float': 'f',
             'double': 'd'}

def read_header(f):
    magic = f.read(4)
    if magic != 'ply\n':
        raise ValueError

    header = {'comments': [], 'elements': []}
    cur_element = None

    while True:
        foo = f.readline().split(' ', 1)
        if foo[0].strip() == 'end_header':
            break

        (key, value) = foo

        if key == 'comment':
            header['comments'].append(value)
        elif key == 'element':
            e = value.split(' ')
            cur_element = {'type': e[0], 'count': int(e[1]), 'properties': []}
            header['elements'].append(cur_element)
        elif key == 'property':
            if cur_element is None:
                raise IndexError('Property found with no element.')

            if cur_element['type'] == 'vertex':
                p = value.split(' ')
                cur_element['properties'].append((p[1].strip(), type_dict[p[0]]))
            else:
                # Ignore faces for now.
                pass

    return header

def ReadPLY():
    f = open('Housahedron_01_9pct.ply', 'rb')
    h = read_header(f)
    vertices = [ ]
    verts = [ ]
    for elem in h['elements']:
        print elem
        if elem['type'] == 'vertex':
            st = ''.join(e[1]  for e in elem['properties'])
            ky = [e[0]  for e in elem['properties']]
            print elem['properties']
            ssz = struct.calcsize(st)
            for i in xrange(elem['count']):
                s = f.read(ssz)
                v = struct.unpack(st, s)
                verts.append(v[:3])
    #           vertices.append(dict(zip(ky, v)))
    #lx = [v[0]  for v in verts]
    #ly = [v[1]  for v in verts]
    return verts

        