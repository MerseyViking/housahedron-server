#!/bin/env python
import sys, os, struct, getopt, string

#  LAS header binary structure:
#  (Description, bytes, struct type, array size)
headerstruct = ( ('filesig', 4,'c',4) ,
               ('filesourceid' , 2,'H',1) ,
               ('reserved'     , 2,'H',1) ,
               ('guid1'        , 4,'L',1) ,
               ('guid2'        , 2,'H',1) ,
               ('guid3'        , 2,'H',1) ,
               ('guid4'        , 8,'B',8) ,
               ('vermajor'     , 1,'B',1) ,
               ('verminor'     , 1,'B',1) ,
               ('sysid'        , 32,'c',32) ,
               ('gensoftware'  , 32,'c',32) ,
               ('fileday'      , 2,'H',1) ,
               ('fileyear'     , 2,'H',1) ,
               ('headersize'   , 2,'H',1) ,
               ('offset'       , 4,'L',1) ,
               ('numvlrecords' , 4,'L',1) ,
               ('pointformat'  , 1,'B',1) ,
               ('pointreclen'  , 2,'H',1) ,
               ('numptrecords' , 4,'L',1) ,
               ('numptbyreturn', 20,'L',5) ,
               ('xscale'       , 8,'d',1) ,
               ('yscale'       , 8,'d',1) ,
               ('zscale'       , 8,'d',1) ,
               ('xoffset'      , 8,'d',1) ,
               ('yoffset'      , 8,'d',1) ,
               ('zoffset'      , 8,'d',1) ,
               ('xmax'         , 8,'d',1) ,
               ('xmin'         , 8,'d',1) ,
               ('ymax'         , 8,'d',1) ,
               ('ymin'         , 8,'d',1) ,
               ('zmax'         , 8,'d',1) ,
               ('zmin'         , 8,'d',1) )


def parseHeader(fh):
    header = { }
    for i in headerstruct:
        if i[2] == 'c':
            value = fh.read(i[1]).strip("\x00")
        elif i[3] > 1:
            value = struct.unpack(str(i[3]) + i[2], fh.read(i[1])) 
        else:
            value = struct.unpack(i[2], fh.read(i[1]))[0]
        header[i[0]] = value
    return header


def getpts(fh, h, numpts=None):
    fh.seek(h['offset'])
    res = [ ]
    xscale, xoffset = h['xscale'], h['xoffset']
    yscale, yoffset = h['yscale'], h['yoffset']
    zscale, zoffset = h['zscale'], h['zoffset']
    pointreclen = h['pointreclen']   # should be 28
    
    fh.seek(h['offset'])
    for i in range(0,numpts):
        #fh.seek(h['offset'] + i * pointreclen)
        srec = fh.read(pointreclen)
        if len(srec) < pointreclen:
            print "rec less than", i, len(rec)
            break
        if (i % 100000) == 0:
            print "pts=", i
        #rec = struct.unpack('=lllHBBbbHd', srec)
        #xrecord, yrecord, zrecord, intensity, return_grp, classification, scan_angle, file_marker, user_fld, gps_time = rec
        rec = struct.unpack('=lll', srec[:12])
        xrecord, yrecord, zrecord = rec

        x = xrecord * xscale + xoffset
        y = yrecord * yscale + yoffset
        z = zrecord * zscale + zoffset

        res.append((x,y,z))

    return res
    
def ReadLAS(infile, n=0):
    fh = open(infile, 'rb')
    header = parseHeader(fh)
    print header
    n = n or header["numptrecords"]
    pts = getpts(fh, header, n)
    fh.close()
    return pts
    
if __name__=='__main__':
    print ReadLAS("Housahedron_01.las", 1000000)[:10]

