# models.py
# Copyright (C) 2013 Housahedron
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import json
import traceback
import re

from datetime import datetime
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

Base = declarative_base()
uuid_pattern = re.compile('[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}\Z', re.I)


class Measurement(Base):
    __tablename__ = 'measurements'

    _id = sqlalchemy.Column(sqlalchemy.Integer, unique=True, autoincrement=True, primary_key=True)
    reading_type = sqlalchemy.Column(sqlalchemy.Text, nullable=False)
    timestamp = sqlalchemy.Column(sqlalchemy.DateTime, index=True, nullable=False)
    value = sqlalchemy.Column(sqlalchemy.Float, nullable=False)
    error = sqlalchemy.Column(sqlalchemy.Text, nullable=True)
    aggregated = sqlalchemy.Column(sqlalchemy.Boolean, nullable=True)
    device_id = sqlalchemy.Column(sqlalchemy.String(36), sqlalchemy.ForeignKey('devices.uuid'), index=True,
                                  nullable=False)

    def __init__(self, reading_type, timestamp, value, aggregated=False):
        self.reading_type = reading_type
        self.timestamp = timestamp
        self.value = value
        self.aggregated = aggregated

    def __repr__(self):
        return '{0} [{1}] {2}'.format(self.timestamp, self.reading_type, self.value)


class Device(Base):
    __tablename__ = 'devices'

    uuid = sqlalchemy.Column(sqlalchemy.String(36), unique=True, primary_key=True)
    metering_point_id = sqlalchemy.Column(sqlalchemy.String(36), sqlalchemy.ForeignKey('metering_points.uuid'),
                                          index=True, nullable=False)
    description = sqlalchemy.Column(sqlalchemy.Text, nullable=True)
    privacy = sqlalchemy.Column(sqlalchemy.Enum('private', 'public'), nullable=False)
    x = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    y = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    z = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    is_placed = sqlalchemy.Column(sqlalchemy.Boolean, nullable=False, default=False)
    measurements = relationship('Measurement', cascade='all', backref=backref('device'))

    def __init__(self, uuid, entity_id, metering_point_id, privacy, description=None):
        if uuid_pattern.match(uuid) is None:
            raise ValueError('Invalid UUID')
        else:
            self.uuid = uuid.lower()

        if uuid_pattern.match(entity_id) is None:
            raise ValueError('Invalid UUID')
        else:
            self.entity_id = entity_id.lower()

        if uuid_pattern.match(metering_point_id) is None:
            raise ValueError('Invalid UUID')
        else:
            self.metering_point_id = metering_point_id.lower()

        self.privacy = privacy.lower()

        self.description = description

    def __repr__(self):
        return self.uuid


class MeteringPoint(Base):
    __tablename__ = 'metering_points'

    uuid = sqlalchemy.Column(sqlalchemy.String(36), unique=True, primary_key=True)
    description = sqlalchemy.Column(sqlalchemy.Text, nullable=True)
    entity_id = sqlalchemy.Column(sqlalchemy.String(36), sqlalchemy.ForeignKey('entities.uuid'), index=True,
                                  nullable=False)
    x = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    y = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    z = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    is_placed = sqlalchemy.Column(sqlalchemy.Boolean, nullable=False, default=False)
    battery_status = sqlalchemy.Column(sqlalchemy.Integer, nullable=False, default=0)
    json_metadata = sqlalchemy.Column(sqlalchemy.Text, nullable=True)
    devices = relationship('Device', cascade='all', backref=backref('metering_point'))

    def __init__(self, uuid, entity_id, description=None, metadata=None):
        if uuid_pattern.match(uuid) is None:
            raise ValueError('Invalid UUID')
        else:
            self.uuid = uuid.lower()

        if uuid_pattern.match(entity_id) is None:
            raise ValueError('Invalid UUID')
        else:
            self.entity_id = entity_id.lower()

        self.description = description
        self.json_metadata = json.dumps(metadata)
        self.is_placed = False
        self.battery_status = 0

    def __repr__(self):
        return self.uuid


class Entity(Base):
    __tablename__ = 'entities'

    uuid = sqlalchemy.Column(sqlalchemy.String(36), unique=True, primary_key=True)
    description = sqlalchemy.Column(sqlalchemy.Text, nullable=True)
    metering_points = relationship('MeteringPoint', cascade='all', backref=backref('entity'))

    def __init__(self, uuid, description=None):
        if uuid_pattern.match(uuid) is None:
            raise ValueError('Invalid UUID')
        else:
            self.uuid = uuid.lower()

        self.description = description

    def __repr__(self):
        return self.uuid


class WeatherObservation(Base):
    __tablename__ = 'observations'

    _id = sqlalchemy.Column(sqlalchemy.Integer, unique=True, autoincrement=True, primary_key=True)
    latitude = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    longitude = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    elevation = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    observation_time = sqlalchemy.Column(sqlalchemy.DateTime, index=True, nullable=False, default=datetime.utcnow())
    weather = sqlalchemy.Column(sqlalchemy.Text, nullable=False, default='')
    temperature = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    rel_humidity = sqlalchemy.Column(sqlalchemy.Integer, nullable=False, default=0)
    wind_dir = sqlalchemy.Column(sqlalchemy.Integer, nullable=False, default=0)
    wind_speed = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    wind_gust = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    pressure = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    dewpoint = sqlalchemy.Column(sqlalchemy.Float, nullable=False, default=0.0)
    precip = sqlalchemy.Column(sqlalchemy.Integer, nullable=False, default=0)

    def __init__(self, obs_text):
        try:
            response = json.loads(obs_text)
            obs = response['current_observation']
            location = obs['observation_location']
            self.latitude = float(location['latitude'])
            self.longitude = float(location['longitude'])
            # Convert elevation to metres.
            self.elevation = float(location['elevation'].split()[0]) / 3.2808399
            self.observation_time = datetime.utcfromtimestamp(int(obs['observation_epoch']))
            self.weather = obs['weather']
            self.temperature = obs['temp_c']
            self.rel_humidity = float(obs['relative_humidity'][:-1])
            self.wind_dir = obs['wind_degrees']
            self.wind_speed = obs['wind_kph']
            self.wind_gust = float(obs['wind_gust_kph'])
            self.pressure = float(obs['pressure_mb'])
            self.dewpoint = obs['dewpoint_c']
            self.precip = int(obs['precip_1hr_metric'])

        except ValueError:
            for frame in traceback.extract_tb(sys.exc_info()[2]):
                fname, lineno, fn, text = frame
                print('Error in {0} on line {1}'.format(fname, lineno))

    def __repr__(self):
        return '{0} {1} degrees C'.format(self.observation_time, self.temperature)
