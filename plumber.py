# plumber.py
# Copyright (C) 2014 Housahedron
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Plumber.

Usage:
    plumber.py [--log=<level>]
    plumber.py (-h | --help)
    plumber.py --version

Options:
    -h --help      Show this help.
    --version      Show version.
    --log=<level>  Log level [default: WARNING].

"""
import base64

import logging
import json
import struct
import uuid

import paho.mqtt.client as mqtt
import ConfigParser
import requests
from datetime import datetime, timedelta
from docopt import docopt
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import models


entity_uuid = None
session = None
wunderground_url = None
# Time in hours before obtaining the next weather observation.
wunderground_retry = 1
last_weather_observation = None
db_connection_string = None
mqttc = None


def init(loglevel):
    conf = ConfigParser.ConfigParser({'db_connection': 'sqlite:///housahedron.db',
                                      'wunderground_api_key': '24a8eca77d4508e3',
                                      'latitude': '53.395319',
                                      'longitude': '-2.972350',
                                      'broker': 'localhost',
                                      'port': '1884'
                                      })

    conf.read(['./plumber.conf', '/home/pi/plumber.conf'])

    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)

    # Stop the requests module from telling us every time it makes a connection.
    logging.getLogger("requests").setLevel(logging.WARNING)

    logging.basicConfig(format='%(asctime)s.%(msecs).03dZ:%(levelname)s:%(message)s',
                        level=numeric_level,
                        datefmt='%Y-%m-%dT%H:%M:%S')

    global db_connection_string
    db_connection_string = conf.get('plumber', 'db_connection')
    engine = create_engine(db_connection_string, echo=False)
    session_factory = sessionmaker(bind=engine)

    global session, wunderground_url
    session = session_factory()

    wunderground_url = 'http://api.wunderground.com/api/{0}/conditions/q/{1},{2}.json'.format(
        conf.get('plumber', 'wunderground_api_key'),
        conf.get('plumber', 'latitude'),
        conf.get('plumber', 'longitude')
    )

    models.Base.metadata.create_all(engine)

    global mqttc
    mqttc = mqtt.Client()
    mqttc.on_message = on_message
    mqttc.connect(conf.get('broker', 'localhost'), int(conf.get('port', '1884')), 60)
    mqttc.subscribe('AMON/#', 0)


def parse_json(json_text):
    try:
        data = json.loads(json_text)
        if 'meteringPoints' in data:
            for metering_point in data['meteringPoints']:
                db_metering_point = session.query(models.MeteringPoint).get(metering_point['meteringPointId'].lower())

                if db_metering_point is None:
                    logging.info('New metering point: {0}'.format(metering_point['meteringPointId'].lower()))
                    db_metering_point = models.MeteringPoint(metering_point['meteringPointId'].lower(),
                                                             metering_point['entityId'].lower(),
                                                             metering_point.get('description'),
                                                             metering_point.get('metadata'))

                    session.add(db_metering_point)

                db_metering_point.battery_status = metering_point['metadata']['battery_status']
                session.commit()
        elif 'devices' in data:
            now = datetime.utcnow()

            for device in data['devices']:
                db_device = session.query(models.Device).get(device['deviceId'].lower())

                if db_device is None:
                    db_device = models.Device(device['deviceId'].lower(), entity_uuid,
                                              device['meteringPointId'].lower(), device['privacy'])
                    session.add(db_device)

                if 'measurements' in device:
                    for measurement in device['measurements']:
                        # Because our JeeNodes don't have an RTC, we need to add the timestamp ourselves.
                        if 'timestamp' not in measurement:
                            measurement['timestamp'] = now

                        db_measurement = models.Measurement(measurement['type'], measurement['timestamp'],
                                                            measurement['value'])
                        db_device.measurements.append(db_measurement)

                        logging.debug(db_measurement)

            session.commit()

    except (ValueError, KeyError):
        session.rollback()
        logging.error('Invalid JSON "{0}"'.format(json_text[:64]))


def register_entity():
    global entity_uuid

    try:
        with open('.plumber_id', 'r') as f:
            the_uuid = f.readall()

        if models.uuid_pattern.match(the_uuid.lower()) is None:
            the_uuid = uuid.uuid4().__str__()

            with open('.plumber_id', 'w') as f:
                f.write(the_uuid)

        entity_uuid = the_uuid

    except IOError:
        entity_uuid = uuid.uuid4().__str__()

    db_entity = session.query(models.Entity).get(entity_uuid)

    if db_entity is None:
        logging.info('New entity: {0}'.format(entity_uuid))
        db_entity = models.Entity(entity_uuid, 'Housahedron')

        session.add(db_entity)
        session.commit()


def on_message(client, user_data, message):
    topic = message.topic.split('/')

    if topic[0] == 'AMON':
        reading_type = topic[1]
        fmt = topic[2]
        metering_point_id = uuid.UUID(base64.urlsafe_b64decode(topic[3])).__str__()

        db_metering_point = session.query(models.MeteringPoint).get(metering_point_id)

        if db_metering_point is None:
            logging.info('New metering point: {0}'.format(metering_point_id))
            db_metering_point = models.MeteringPoint(metering_point_id, entity_uuid)

            session.add(db_metering_point)

        payload = struct.unpack(fmt, message.payload)

        if reading_type == 'temperatureAir':
            aggregate = payload[0]
            # timestamp = payload[1]
            values = payload[2:]
            i = 0

            for value in values:
                device_id = '{0}9{1}{2:02x}'.format(metering_point_id[:19], metering_point_id[20:-2], i)
                db_device = session.query(models.Device).get(device_id)

                if db_device is None:
                    db_device = models.Device(device_id, entity_uuid, metering_point_id, 'private')
                    session.add(db_device)

                db_measurement = models.Measurement(reading_type, datetime.utcnow(), value, (aggregate & 1) is not 0)
                db_device.measurements.append(db_measurement)
                aggregate >>= 1

                logging.debug(db_measurement)

        elif reading_type == 'status':
            # TODO: Detect when the battery has been changed by calculating how long its been since the last battery
            # low message.
            db_metering_point.battery_status = payload[0]

        session.commit()


def get_weather():
    global last_weather_observation
    global wunderground_retry

    last_weather_observation = datetime.utcnow()
    try:
        response = requests.get(wunderground_url)
        w = models.WeatherObservation(response.content)
        session.add(w)
        session.commit()
        wunderground_retry = 1
    except requests.ConnectionError:
        wunderground_retry *= 2
        if wunderground_retry > 64:
            logging.warning('Unable to connect to wunderground.com after several attempts. Giving up.')
        else:
            logging.info('Unable to connect to wunderground.com '
                         'Next retry will be in {0} hours.'.format(wunderground_retry))


if __name__ == '__main__':
    arguments = docopt(__doc__, version='Plumber 3.0')

    init(arguments['--log'])

    register_entity()

    # Force an immediate get of the latest weather data.
    get_weather()

    try:
        while True:
            mqttc.loop()

            if wunderground_retry <= 64 and \
               (datetime.utcnow() - last_weather_observation) > timedelta(hours=wunderground_retry):
                get_weather()

    except KeyboardInterrupt:
        mqttc.disconnect()